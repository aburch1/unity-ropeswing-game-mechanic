﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsMenu : MonoBehaviour {

    [SerializeField]
    private Slider masterVolSlider, musicVolSlider, sfxVolSlider;

    void OnEnable()
    {
        if (masterVolSlider != null)
            masterVolSlider.value = AudioManager.instance.masterVolumeSlider;
        if (musicVolSlider != null)
            musicVolSlider.value = AudioManager.instance.musicVolumeSlider;
        if (sfxVolSlider != null)
            sfxVolSlider.value = AudioManager.instance.sfxVolumeSlider;
    }
}
