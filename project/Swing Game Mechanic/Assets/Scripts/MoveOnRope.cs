﻿using UnityEngine;
using System.Collections;

public class MoveOnRope : MonoBehaviour {

    private ConnectTo connectToScript;
    public float climbPause = 1f;

    private Animator a;

    public int currentPos = 22;
    private int maxPos = 24;
    private int minPos = 3;

    private float previousClimbTime;

	// Use this for initialization
	void Start () {
        connectToScript = GetComponent<ConnectTo>();
        previousClimbTime = Time.time;
        a = GameObject.FindWithTag("PlayerModel").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && currentPos > minPos)
        {
            if (connectToScript.connectToObject != null)
                ClimbUp();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f && currentPos < maxPos)
        {
            if (connectToScript.connectToObject != null)
                ClimbDown();
        }
        else
        {
            a.SetBool("Climbing", false);
        }
	}

    private void ClimbUp()
    {
        if (GameManager.instance.TutorialActive())
        {
            if (!Tutorial.instance.climbOnRope && Tutorial.instance.CurrentSection() > 12)
            {
                Tutorial.instance.ClimbOnRope();
            }
        }
        
        GameObject point = connectToScript.connectToObject.transform.parent.gameObject;
        connectToScript.connectToObject = point;
        currentPos--;
        previousClimbTime = Time.time;
        a.SetBool("Climbing", true);
    }

    private void ClimbDown()
    {
        GameObject point = connectToScript.connectToObject.transform.GetChild(0).gameObject;
        connectToScript.connectToObject = point;
        currentPos++;
    }
}
