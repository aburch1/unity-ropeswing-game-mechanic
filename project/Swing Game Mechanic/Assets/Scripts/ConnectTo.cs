﻿using UnityEngine;
using System.Collections;

public class ConnectTo : MonoBehaviour {

    private RopeCatch ropeCatchScript;

    public GameObject connectToObject;
    [SerializeField]
    private float distFromRope = 1f;
    [SerializeField]
    private float sideSwingDist = 0.5f;
    [SerializeField]
    private float swingForce;
    [SerializeField]
    private float jumpMultiplier = 1;

    private bool jumped = false;

    private Animator a;

    private bool rightSideSwing = false;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        a = GameObject.FindWithTag("PlayerModel").GetComponent<Animator>();
        ropeCatchScript = GetComponent<RopeCatch>();
    }

    void FixedUpdate () {
	    if (connectToObject != null && transform.position != connectToObject.transform.position)
        {
            if (!rightSideSwing)
            {
                transform.position = connectToObject.transform.position - transform.forward * distFromRope - transform.right * sideSwingDist;
                transform.localScale = new Vector3(1f, 1f, 1f);
            } 
            else
            {
                transform.position = connectToObject.transform.position - transform.forward * distFromRope + transform.right * sideSwingDist;
                transform.localScale = new Vector3(-1f, 1f, 1f);
            } 
        }

        float zMove = Input.GetAxis("Vertical");

        if (connectToObject != null)
        {
            if (GameManager.instance.TutorialActive())
            {
                if (zMove > 0.9f && !Tutorial.instance.swungRope)
                {
                    Tutorial.instance.SwungOnRope();
                }
            }
            connectToObject.GetComponent<Rigidbody>().AddForce(transform.forward * zMove * swingForce, ForceMode.Force);
        }

        if (jumped)
        {
            jumped = false;
            if (GameManager.instance.TutorialActive())
            {
                if (!Tutorial.instance.jumpedOffRope)
                {
                    Tutorial.instance.JumpedOffRope();
                }
            }

            Vector3 currVelocity = connectToObject.GetComponent<Rigidbody>().velocity;
            connectToObject = null;
            ropeCatchScript.sectionToCatch = null;
            Invoke("ResetRope", 1f);
            rb.useGravity = true;
            rb.velocity = currVelocity;
            rb.AddForce(transform.up * 200 * jumpMultiplier);
            rb.AddForce(transform.forward * zMove * 80 * jumpMultiplier);
            a.SetBool("CurrOnRope", false);
            a.SetTrigger("OffRope");
        }
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && connectToObject != null)
        {
            jumped = true;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rightSideSwing = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            rightSideSwing = false;
        }

    }

    private void ResetRope()
    {
        ropeCatchScript.previousRope = "";
    }
}
