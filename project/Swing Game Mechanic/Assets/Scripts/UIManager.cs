﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIManager : MonoBehaviour {

    public static UIManager instance = null;

    [SerializeField]
    private GameObject infoPanel;
    [SerializeField]
    private Tutorial tutorialScript;
    [SerializeField]
    private Text promptText, berryBushText, infoText;
    [SerializeField]
    private Image[] healthHearts;

    private Color heartsOn = new Color(255f, 255f, 255f, 1f);
    private Color heartsOff = new Color(255f, 255f, 255f, 0.3f);

    void Awake () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
	}

    void Start()
    {
        if (!GameManager.instance.TutorialActive() && infoPanel.activeInHierarchy)
        {
            HideTutorial();
        }
    }

    public void ShowPromptText(string textToShow)
    {
        promptText.text = textToShow;
        promptText.gameObject.SetActive(true);
    }

    public void HidePromptText()
    {
        promptText.gameObject.SetActive(false);
    }

    public void UpdateUI()
    {
        berryBushText.text = ItemsToCollect.instance.berriesCollected + " / " + ItemsToCollect.instance.maxBerries;

        switch (GameManager.instance.getPlayerHealth())
        {
            case 3:
                {
                    healthHearts[0].color = heartsOn;
                    healthHearts[1].color = heartsOn;
                    healthHearts[2].color = heartsOn;
                    break;
                }
            case 2:
                {
                    healthHearts[0].color = heartsOff;
                    healthHearts[1].color = heartsOn;
                    healthHearts[2].color = heartsOn;
                    break;
                }
            case 1:
                {
                    healthHearts[0].color = heartsOff;
                    healthHearts[1].color = heartsOff;
                    healthHearts[2].color = heartsOn;
                    break;
                }
            case 0:
                {
                    GameManager.instance.LoseGame();
                    break;
                }
        }
    }

    public void UpdateInfoText(string text)
    {
        infoText.text = text;
    }

    public void HideTutorial()
    {
        infoPanel.SetActive(false);
        GameManager.instance.TutorialActive(false);
        tutorialScript.enabled = false;
    }
}
