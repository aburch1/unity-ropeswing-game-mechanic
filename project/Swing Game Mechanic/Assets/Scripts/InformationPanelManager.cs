﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

public class InformationPanelManager : MonoBehaviour {

    [SerializeField]
    private GameObject infoPanel;

    [SerializeField]
    private RigidbodyFirstPersonController playerControlScript;
    [SerializeField]
    private ShootRope shootRopeScript;

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Cancel") && infoPanel.activeInHierarchy)
        {
            if (playerControlScript == null || shootRopeScript == null)
                FindScripts();
            infoPanel.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.None;
            playerControlScript.enabled = true;
            shootRopeScript.enabled = true;
            Time.timeScale = 1f;
        }
        else if (CrossPlatformInputManager.GetButtonDown("Cancel") && !infoPanel.activeInHierarchy)
        {
            if (playerControlScript == null || shootRopeScript == null)
                FindScripts();
            infoPanel.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            playerControlScript.enabled = false;
            shootRopeScript.enabled = false;
            Time.timeScale = 0f;
        }
    }


    private void FindScripts()
    {
        playerControlScript = GameObject.FindWithTag("Player").GetComponent<RigidbodyFirstPersonController>();
        shootRopeScript = GameObject.FindWithTag("Player").GetComponent<ShootRope>();
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
