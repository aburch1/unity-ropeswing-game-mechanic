﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour {

    private AudioSource a;

    [SerializeField]
    private GameObject objToPickup;
    [SerializeField]
    private Light spotLight;

    [SerializeField]
    private enum ItemType {RopeShooter, Berries}
    [SerializeField]
    private ItemType myItemType;

    private bool canInteract = false;

    private bool itemAvailable = true;
	
    void Start()
    {
        a = GetComponent<AudioSource>();
    }

	void Update () {
	    if (canInteract && Input.GetKeyDown(KeyCode.E) && itemAvailable)
        {
            itemAvailable = false;
            objToPickup.SetActive(false);
            spotLight.gameObject.SetActive(false);
            ProcessPickup();
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && itemAvailable)
        {
            canInteract = true;
            UIManager.instance.ShowPromptText("E");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            canInteract = false;
            UIManager.instance.HidePromptText();
        }
    }

    private void ProcessPickup()
    {
        if (GameManager.instance.TutorialActive())
        {
            if (!Tutorial.instance.eatBerries)
            {
                Tutorial.instance.EatBerries();
            }
        }

        switch (myItemType)
        {
            case ItemType.Berries : ItemsToCollect.instance.berriesCollected++; GameManager.instance.changePlayerHealth(1); break;
            case ItemType.RopeShooter: ItemsToCollect.instance.hasRopeShooter = true; break;
            default: Debug.Log("Item Type not implemented"); break;
        }

        itemAvailable = false;
        a.Play();
        UIManager.instance.HidePromptText();
        UIManager.instance.UpdateUI();
    }
}
