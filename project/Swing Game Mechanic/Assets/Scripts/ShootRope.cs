﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ShootRope : MonoBehaviour {

    private ConnectTo connectToScript;
    private RopeCatch ropeCatchScript;
    private RopeMove ropeMoveScript;

    [SerializeField]
    private Image reticule;
    private Color orangeReticule = new Color(227, 127, 0);
    private Color purpleReticule = new Color(81, 45, 0);

    public Transform cam;
    private Rigidbody rb;

    [SerializeField]
    private GameObject[] ropeCoils;
    private RopeMove[] ropeCoilMoverScript = new RopeMove[2];
    [SerializeField]
    private Transform ropeSpawnPoint;

    private Animator a;

    [SerializeField]
    private GameObject orangeRope;
    [SerializeField]
    private GameObject purpleRope;

    private int shotNumber = 0;

    private GameObject ropeCoilToShoot;
    private RopeMove ropeCoilMover;

    [SerializeField][Range(0f, 1f)]
    private float triggerSensitivity = 0.8f;

    private bool shooting = false;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        connectToScript = GetComponent<ConnectTo>();
        ropeCatchScript = GetComponent<RopeCatch>();
        a = GameObject.FindWithTag("PlayerModel").GetComponent<Animator>();

        orangeRope = Instantiate(orangeRope) as GameObject;
        purpleRope = Instantiate(purpleRope) as GameObject;
        orangeRope.SetActive(false);
        purpleRope.SetActive(false);

        for(int i = 0; i < 2; i++)
        {
            ropeCoils[i] = Instantiate(ropeCoils[i]) as GameObject;
            ropeCoilMoverScript[i] = ropeCoils[i].GetComponent<RopeMove>();
            ropeCoils[i].SetActive(false);
        } 
	}

	
	// Update is called once per frame
	void Update () {

	    if (CrossPlatformInputManager.GetButtonDown("Fire1") || CrossPlatformInputManager.GetAxis("Fire1") < (triggerSensitivity -1))
        {
            if (connectToScript.connectToObject == null)
            {
                ChooseRopeCoil();
                shooting = true;
            }
            else
            {
                Debug.Log("You can not shoot while on a rope");
            }
            
        }

        if ((CrossPlatformInputManager.GetButtonUp("Fire1") || CrossPlatformInputManager.GetAxis("Fire1") == 0) && shooting)
        {
            if (ropeCoilToShoot != null && ropeCoilMover != null)
                ropeCoilMover.ShootRope();

            shooting = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetRigidBodies(orangeRope);
            orangeRope.SetActive(false);
            orangeRope.transform.SetParent(null);
            ResetRigidBodies(purpleRope);
            purpleRope.SetActive(false);
            purpleRope.transform.SetParent(null);
        }
    }
    
    public void ChooseRope(GameObject hookPoint)
    {
        Transform ropePoint = hookPoint.transform.GetChild(0);
        if (ropePoint.childCount <= 0)
        {
            if (shotNumber % 2 == 0)
            {
                SpawnRopeSwing(orangeRope, ropePoint, purpleReticule);
            }
            else
            {
                SpawnRopeSwing(purpleRope, ropePoint, orangeReticule);
            }
        }
    }

    private void SpawnRopeSwing(GameObject rope, Transform ropePoint, Color reticuleColor)
    {
        if (rope.activeInHierarchy)
            rope.SetActive(false);

        rope.transform.position = ropePoint.position;
        rope.transform.SetParent(ropePoint);
        shotNumber++;
        if (shotNumber >= 2)
            shotNumber = 0;


        connectToScript.connectToObject = null;
        rb.useGravity = true;
        a.SetBool("CurrOnRope", false);
        reticule.color = reticuleColor;
        rope.SetActive(true);
    }

    private void ChooseRopeCoil()
    {
        for (int i = 0; i < ropeCoils.Length; i++)
        {
            if (!ropeCoils[i].activeInHierarchy)
            {
                ropeCoilToShoot = ropeCoils[i];
                ropeCoilMover = ropeCoilMoverScript[i];
                break;
            }
            else
            {
                ropeCoilToShoot = null;
                ropeCoilMover = null;
            }
        }

        if (ropeCoilToShoot != null)
        {
            ropeCoilToShoot.transform.position = ropeSpawnPoint.position;
            ropeCoilToShoot.transform.rotation = ropeSpawnPoint.localRotation;
            ropeCoilToShoot.transform.SetParent(ropeSpawnPoint);
            ropeCoilToShoot.SetActive(true);
        }

        a.SetTrigger("ChargeThrow");

        ropeCatchScript.previousRope = "";
    }

    private void ResetRigidBodies(GameObject parent)
    {
        Rigidbody[] children = parent.GetComponentsInChildren<Rigidbody>();

        for (int i = 0; i < children.Length; i++)
        {
            ResetVelocity(children[i]);
            Debug.Log("Finished Resetting");
        }
    }

    IEnumerator ResetVelocity(Rigidbody rb)
    {
        Debug.Log("Reseting Velocity");
        rb.velocity = Vector3.zero;

        if (rb.velocity != Vector3.zero)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            ResetVelocity(rb);
        }
    }
}
