﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class levelSelectLoader : MonoBehaviour {

    private GameObject levelLockedImage;
    [SerializeField]
    private bool locked = true;
    [SerializeField]
    private int levelNumber;

    [SerializeField]
    private Image[] berries = new Image[3];

    private Color berriesOn = new Color(255f, 255f, 255f, 1f);
    private Color berriesOff = new Color(255f, 255f, 255f, 0.2f);

    void Start () {
        levelLockedImage = transform.GetChild(4).gameObject;

        if (levelNumber - 1 <= GameManager.instance.LevelsComplete())
        {
            Debug.Log("Passed Lockcheck");
            levelLockedImage.SetActive(false);
            locked = false;
        }

        int levelScore = GameManager.instance.GetLevelScore(levelNumber);

        for (int i = 0; i < levelScore; i++)
        {
            berries[i].color = berriesOn;
        }
        
        for (int i = levelScore; i < berries.Length; i++)
        {
            berries[i].color = berriesOff;
        }
    }

    public void LoadLevel()
    {
        Debug.Log("Called");
        if (!locked)
        {
            Debug.Log("Button Pressed");
            GameManager.instance.LoadLevel("Level " + levelNumber);
        }
        else
        {
            Debug.Log("Locked");
        }
    }
}
