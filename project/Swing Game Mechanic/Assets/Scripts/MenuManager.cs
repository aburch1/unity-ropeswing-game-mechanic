﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour {

    [SerializeField]
    private Slider masterVolSlider, musicVolSlider, sfxVolSlider;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        if (masterVolSlider != null)
            masterVolSlider.value = AudioManager.instance.masterVolumeSlider;
        if (musicVolSlider != null)
            musicVolSlider.value = AudioManager.instance.musicVolumeSlider;
        if (sfxVolSlider != null)
            sfxVolSlider.value = AudioManager.instance.sfxVolumeSlider;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level Select");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void ResetGame()
    {
        GameManager.instance.ResetCompletedLevels();
        MainMenu();
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void ChangeMasterVolume(float volume)
    {
        AudioManager.instance.SetMasterVolume(volume);
    }

    public void ChangeMusicVolume(float volume)
    {
        AudioManager.instance.SetMusicVolume(volume);
    }

    public void ChangeSFXVolume(float volume)
    {
        AudioManager.instance.SetSFXVolume(volume);
    }
}
