﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    [SerializeField]
    private int numberOfLevels = 2;

    private int playerMaxHealth = 3;
    private int playerHealth = 3;

    private int[] levelScores = new int[6];
    
    [SerializeField]
    private int levelsComplete = 0;

    private bool tutorialActive = true;

	void Awake () {

	    if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        ResetCompletedLevels();   
	}

    public void changePlayerHealth(int amount)
    {
        playerHealth += amount;

        if (playerHealth > playerMaxHealth)
            playerHealth = playerMaxHealth;
        else if (playerHealth < 0)
            playerHealth = 0;

        UIManager.instance.UpdateUI();
    }

    public int getPlayerHealth()
    {
        return playerHealth;
    }

    public void LoseGame()
    {
        changePlayerHealth(3);
        ItemsToCollect.instance.berriesCollected = 0;
        SceneManager.LoadScene("Lose");
    }

    public int LevelsComplete()
    {
        return levelsComplete;
    }

    public void CompleteLevel(int levelNum)
    {
        if (ItemsToCollect.instance.berriesCollected > levelScores[levelNum-1])
        {
            levelScores[levelNum - 1] = ItemsToCollect.instance.berriesCollected;
        }
        levelsComplete++;
        if (levelsComplete >= numberOfLevels)
            levelsComplete -= 1;
        ResetBerries();
        ResetHealth();
        SceneManager.LoadScene("Level Select");
    }

    public void ResetHealth()
    {
        playerHealth = playerMaxHealth;
    }

    public void ResetBerries()
    {
        ItemsToCollect.instance.berriesCollected = 0;
    }

    public void ResetCompletedLevels()
    {
        levelsComplete = 0;
        for (int i = 0; i < levelScores.Length; i++)
        {
            levelScores[i] = 0;
        }
    }

    public void LoadLevel(string levelName)
    {
        if (levelsComplete > 0)
        {
            TutorialActive(false);
        }
        else
        {
            TutorialActive(true);
        }

        SceneManager.LoadScene(levelName);
    }

    public void TutorialActive(bool status)
    {
        tutorialActive = status;
    }

    public bool TutorialActive()
    {
        return tutorialActive;
    }

    public int GetLevelScore(int levelNum)
    {
        return levelScores[levelNum - 1];
    }
}
