﻿using UnityEngine;
using System.Collections;

public class ResetRopePhysics : MonoBehaviour
{
    [SerializeField]
    private Rigidbody bone1;

    [SerializeField]
    private Rigidbody[] boneRbs = new Rigidbody[24];

    void Start()
    {
        boneRbs[0] = bone1;

        for (int i = 1; i < boneRbs.Length; i++)
        {
            boneRbs[i] = boneRbs[i - 1].transform.GetChild(0).gameObject.GetComponent<Rigidbody>();
        }
    }

    void FixedUpdate()
    {
        if (!gameObject.activeInHierarchy)
        {
            for (int i = 0; i < boneRbs.Length; i++)
            {
                boneRbs[i].velocity = Vector3.zero;
                boneRbs[i].angularVelocity = Vector3.zero;
            }
        }
    }
}   
