﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour {

    public float time = 5f;

	// Use this for initialization
	void Start () {
        Invoke("DeleteObj", time);
	}
	
	public void DeleteObj()
    {
        Destroy(gameObject);
    }
}
