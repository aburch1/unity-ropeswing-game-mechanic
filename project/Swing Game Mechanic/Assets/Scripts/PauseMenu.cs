﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    [SerializeField]
    private GameObject pauseMenuPanel;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject optionsMenu;
    [SerializeField]
    private RigidbodyFirstPersonController fpController;
    [SerializeField]
    private ShootRope shootRope;

    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Cancel"))
        {
            if (Time.timeScale == 0f)
            {
                if (optionsMenu.activeInHierarchy)
                    HideOptionsMenu();
                else
                    ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    private void PauseGame()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        Time.timeScale = 0f;
        pauseMenuPanel.SetActive(true);
        pauseMenu.SetActive(true);
        optionsMenu.SetActive(false);
        fpController.enabled = false;
        shootRope.enabled = false;
    }

    public void QuitToMenu()
    {
        Time.timeScale = 1f;
        GameManager.instance.ResetHealth();
        GameManager.instance.ResetBerries();
        GameManager.instance.LoadLevel("Main Menu");
    }

    public void ShowOptionsMenu()
    {
        optionsMenu.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void HideOptionsMenu()
    {
        optionsMenu.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        Time.timeScale = 1f;
        pauseMenuPanel.SetActive(false);
        pauseMenu.SetActive(false);
        fpController.enabled = true;
        shootRope.enabled = true;
    }

    public void TurnOffTutorial()
    {
        if (GameManager.instance.TutorialActive())
        {
            UIManager.instance.HideTutorial();
        }
    }

    public void ChangeMasterVolume(float volume)
    {
        AudioManager.instance.SetMasterVolume(volume);
    }

    public void ChangeMusicVolume(float volume)
    {
        AudioManager.instance.SetMusicVolume(volume);
    }

    public void ChangeSFXVolume(float volume)
    {
        AudioManager.instance.SetSFXVolume(volume);
    }
}
