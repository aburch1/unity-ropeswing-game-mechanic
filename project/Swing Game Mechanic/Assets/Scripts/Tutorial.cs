﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

    public static Tutorial instance = null;
    
    private string[] tutorialText = new string[17];
    [SerializeField]
    private float autoAdvanceTime = 5f;
    [SerializeField]
    private GameObject firstWhiteCrystalParticles, secondWhiteCrystalParticles, thirdWhiteCrystalParticles, fourthWhiteCrystalParticles;

    public bool usedTeleport, firstWhiteCrystal, secondWhiteCrystal, grabedRope, swungRope, jumpedOffRope, eatBerries, thirdWhiteCrystal, fourthWhiteCrystal, climbOnRope, checkpoint = false;

    private int currentSection = 0;

    private float lastInfoTime;
    [SerializeField]
    private float minimumDisplayTime = 2f;
    
	void Start () {

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        tutorialText[0] = "Welcome to rope swing!\n\nmy name is George, i'll be helping to\nget you started here!";
        tutorialText[1] = "See that blue crystal up there?\n\nTry throwing a rope at it using\nthe left mouse button!";
        tutorialText[2] = "Nice one you did it! If you see any\nblue crystals you can use them to teleport\nright to them!";
        tutorialText[3] = "Now try throwing a rope at the\nclosest white crystal to the right!\n\n(pssst..the one with the particles)";
        tutorialText[4] = "Before you jump onto it also throw a rope\nat the crystal behind it!\n\nIf you hit the wrong crystals just press R\nto get the ropes back!";
        tutorialText[5] = "You really are a natural at this!\nSee if you can jump and grab the rope, you\nwill grab on automatically if you get\nclose enough!\nPress Space to jump!";
        tutorialText[6] = "You can swing back and forth on the rope\nusing the W and S keys!\nGive it a go";
        tutorialText[7] = "Now you're really getting into the\nSWING of things!";
        tutorialText[8] = "Try swinging into the next rope\nand then swinging and jumping\nto the platform using space!";
        tutorialText[9] = "Oh look a berry bush!! Try and\ncollect all of the berries throughout\nthe level to get the highest score!";
        tutorialText[10] = "I guess you could eat them instead...\n\nRight you know the drill throw a\nrope at the crystal with the particles!";
        tutorialText[11] = "And the one behind it!\n\nYou must remeber to throw your ropes before\njumping onto them as you can't throw\na rope while hanging on one!";
        tutorialText[12] = "Notice your first two ropes disapeared?!\nYou are only ever allowed two ropes active\nSo choose your routes carefully!";
        tutorialText[13] = "Hmm, the second rope seems quite high up...\nTry climbing up and down the rope using the\nmouse scroll wheel!";
        tutorialText[14] = "Here is a checkpoint, activate it\nusing the E key.\n\nIf you do fall off and lose a health\nyou will respawn at your last activated\ncheckpoint!";
        tutorialText[15] = "Seems like you've got the hang of this!\n\nThats all I can really show you for now!\nGood luck getting through the cave and\ncollecting the berries!";
        tutorialText[16] = "Tutorial Finished";

        UIManager.instance.UpdateInfoText(tutorialText[currentSection]);
        Invoke("CompletedSection", autoAdvanceTime);
        lastInfoTime = Time.time;
	}
	
	public int CurrentSection()
    {
        return currentSection;
    }

    public void CompletedSection()
    {
        if (lastInfoTime + minimumDisplayTime > Time.time)
        {
            Debug.Log("Invoked completesection");
            Invoke("CompletedSection", minimumDisplayTime);
        }
        else
        {
            currentSection++;
            UIManager.instance.UpdateInfoText(tutorialText[currentSection]);
            CheckForIndicators();
            lastInfoTime = Time.time;
        }
    }

    public void UseTeleport()
    {
        usedTeleport = true;
        CompletedSection();
        Invoke("CompletedSection", autoAdvanceTime);
    }

    public void UseFirstWhiteCrystal()
    {
        firstWhiteCrystal = true;
        firstWhiteCrystalParticles.SetActive(false);
        CompletedSection();
    }

    public void UseSecondWhiteCrystal()
    {
        secondWhiteCrystal = true;
        secondWhiteCrystalParticles.SetActive(false);
        CompletedSection();
    }

    public void UseThirdWhiteCrystal()
    {
        thirdWhiteCrystal = true;
        thirdWhiteCrystalParticles.SetActive(false);
        CompletedSection();
    }

    public void UseFourthWhiteCrystal()
    {
        fourthWhiteCrystal = true;
        fourthWhiteCrystalParticles.SetActive(false);
        CompletedSection();
        Invoke("CompletedSection", autoAdvanceTime);
    }

    public void GrabedOntoRope()
    {
        grabedRope = true;
        CompletedSection();
    }

    public void SwungOnRope()
    {
        swungRope = true;
        CompletedSection();
        Invoke("CompletedSection", autoAdvanceTime);
    }

    public void JumpedOffRope()
    {
        jumpedOffRope = true;
        CompletedSection();
    }

    public void EatBerries()
    {
        eatBerries = true;
        CompletedSection();
    }

    public void ClimbOnRope()
    {
        climbOnRope = true;
        CompletedSection();
    }

    public void Checkpoint()
    {
        checkpoint = true;
        CompletedSection();
        Invoke("HideTutorial", autoAdvanceTime + autoAdvanceTime);
    }

    private void HideTutorial()
    {
        UIManager.instance.HideTutorial();
        CompletedSection();
    }

    private void CheckForIndicators()
    {
        switch (currentSection)
        {
            case 3:
                {
                    firstWhiteCrystalParticles.SetActive(true);
                    break;
                }
            case 4:
                {
                    secondWhiteCrystalParticles.SetActive(true);
                    break;
                }
            case 10:
                {
                    thirdWhiteCrystalParticles.SetActive(true);
                    break;
                }
            case 11:
                {
                    fourthWhiteCrystalParticles.SetActive(true);
                    break;
                }
        }
    }
}
