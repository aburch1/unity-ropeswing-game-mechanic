﻿using UnityEngine;
using System.Collections;

public class ExitLevel : MonoBehaviour {

    [SerializeField]
    private int levelNumber;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.instance.CompleteLevel(levelNumber);
        }
    }
}
