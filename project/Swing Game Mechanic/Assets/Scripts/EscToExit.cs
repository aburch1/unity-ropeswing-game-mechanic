﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class EscToExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (CrossPlatformInputManager.GetButtonDown("Cancel"))
        {
            SceneManager.LoadScene("Main Menu");
        }
	}
}
