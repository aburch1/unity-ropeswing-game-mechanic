﻿using UnityEngine;
using System.Collections;

public class RopeMove : MonoBehaviour {

    private AudioSource audioS;

    public float deleteTimer = 5f;

    private ShootRope shootRopeScript;

    public float throwSpeed;

    private Animator a;

    private Transform cam;

    private Rigidbody rb;

    private GameObject player;
    private Rigidbody playerRB;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = GameObject.FindWithTag("MainCamera").transform;
        shootRopeScript = GameObject.FindWithTag("Player").GetComponent<ShootRope>();
        a = GameObject.FindWithTag("PlayerModel").GetComponent<Animator>();
        player = GameObject.Find("Player");
        playerRB = player.GetComponent<Rigidbody>();
        audioS = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (gameObject.transform.parent != null)
        {
            rb.velocity = playerRB.velocity;
        }
    }

    void OnDisable()
    {
        CancelInvoke("RemoveObj");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "HookPoint")
        {
            if (other.transform.GetChild(0).childCount <= 0)
            {
                if (GameManager.instance.TutorialActive())
                {
                    if (!Tutorial.instance.firstWhiteCrystal && Tutorial.instance.CurrentSection() >= 3)
                    {
                        Tutorial.instance.UseFirstWhiteCrystal();
                    }
                    else if (!Tutorial.instance.secondWhiteCrystal && Tutorial.instance.CurrentSection() >= 4)
                    {
                        Tutorial.instance.UseSecondWhiteCrystal();
                    }
                    else if (!Tutorial.instance.thirdWhiteCrystal && Tutorial.instance.CurrentSection() >= 10)
                    {
                        Tutorial.instance.UseThirdWhiteCrystal();
                    }
                    else if (!Tutorial.instance.fourthWhiteCrystal && Tutorial.instance.CurrentSection() >= 11)
                    {
                        Tutorial.instance.UseFourthWhiteCrystal();
                    }
                }

                shootRopeScript.ChooseRope(other.gameObject);
                CancelInvoke("RemoveObj");
                RemoveObj();
            }
        }
        else if (other.gameObject.tag == "TelePoint")
        {
            if (GameManager.instance.TutorialActive())
            {
                if (!Tutorial.instance.usedTeleport)
                {
                    Tutorial.instance.UseTeleport();
                }
            }
            player.transform.position = other.transform.position + new Vector3(0f, -3f, 0f);
            CancelInvoke("RemoveObj");
            RemoveObj();
        }
        else if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Floor")
        {
            CancelInvoke("RemoveObj");
            RemoveObj();
        }
    }

    public void ShootRope()
    {
        player.GetComponent<Rigidbody>().isKinematic = false;
        transform.parent = null;
        rb.velocity = Vector3.zero;
        rb.AddForce(cam.forward * throwSpeed, ForceMode.Acceleration);
        Invoke("RemoveObj", deleteTimer);
        a.SetTrigger("ThrowRope");
        audioS.Play();
    }

    private void RemoveObj()
    {
        gameObject.SetActive(false);
        rb.velocity = Vector3.zero;
    }
}
