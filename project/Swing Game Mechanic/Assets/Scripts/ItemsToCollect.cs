﻿using UnityEngine;
using System.Collections;

public class ItemsToCollect : MonoBehaviour
{

    public static ItemsToCollect instance = null;

    public bool hasRopeShooter = false;
    public int berriesCollected = 0;
    public int maxBerries;

    void Start()
    {
        if (instance == null)
            instance = this;
    }
}
