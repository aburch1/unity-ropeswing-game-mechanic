﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

    private Light spotLight;
    private GameObject spawnPoint;

    private bool canInteract = false;

	// Use this for initialization
	void Start () {
        spawnPoint = transform.GetChild(0).gameObject;
        spotLight = transform.GetChild(1).GetComponent<Light>();

        spotLight.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
	    if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            if (GameManager.instance.TutorialActive())
            {
                if (!Tutorial.instance.checkpoint)
                {
                    Tutorial.instance.Checkpoint();
                }
            }
            ResetPlayer.instance.setResetPoint(spawnPoint);
            spotLight.color = Color.green;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            canInteract = true;
            UIManager.instance.ShowPromptText("E");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            canInteract = false;
            UIManager.instance.HidePromptText();
        }
    }
}
