﻿using UnityEngine;
using System.Collections;

public class ResetPlayer : MonoBehaviour {

    public static ResetPlayer instance = null;

    private AudioSource audioSource;

    private GameObject player;
    private Rigidbody playerRB;

    [SerializeField]
    private GameObject resetPoint;
    [SerializeField]
    private float moveTime = 5f;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

	void Start () {
        audioSource = GetComponent<AudioSource>();

        player = GameObject.FindWithTag("Player");
        playerRB = player.GetComponent<Rigidbody>();
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Player")
        {
            audioSource.Play();
            GameManager.instance.changePlayerHealth(-1);
            StartCoroutine(MoveToOverSeconds(other.gameObject, resetPoint, moveTime));
        }
    }

    public void setResetPoint(GameObject point)
    {
        resetPoint = point;
    }

    public IEnumerator MoveToOverSeconds(GameObject objToMove, GameObject endObj, float seconds)
    {
        float elapsedTime = 0f;
        Vector3 startingPos = objToMove.transform.position;
        Vector3 endPos = endObj.transform.position;

        playerRB.useGravity = false;

        while(elapsedTime < seconds)
        {
            objToMove.transform.position = Vector3.Lerp(startingPos, endPos, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objToMove.transform.position = endPos;
        playerRB.useGravity = true;
    }
}
