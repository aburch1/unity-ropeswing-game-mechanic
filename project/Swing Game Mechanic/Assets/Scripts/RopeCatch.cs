﻿using UnityEngine;
using System.Collections;

public class RopeCatch : MonoBehaviour {

    private MoveOnRope moveOnRopeScript;
    private ConnectTo connectToScript;

    private Animator a;

    public GameObject sectionToCatch;
    private Rigidbody rb;
    private int sectionPos;

    public string previousRope = "";

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        moveOnRopeScript = GetComponent<MoveOnRope>();
        connectToScript = GetComponent<ConnectTo>();
        a = GameObject.FindWithTag("PlayerModel").GetComponent<Animator>();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "OrangeRope")
        {
            CheckRope(other);
        }

        if (other.gameObject.tag == "PurpleRope")
        {
            CheckRope(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Rope")
        {
            Invoke("resetSectionToCatch", 1f);
        }
    }

    private void CheckRope(Collider other)
    {
        sectionToCatch = other.gameObject;
        string[] name = new string[2];
        name = other.gameObject.name.Split(':');
        sectionPos = int.Parse(name[1]);
        if (previousRope != other.gameObject.tag)
        {
            CatchRope();
            previousRope = other.gameObject.tag;
        }
    }

    public void CatchRope()
    {
        if (GameManager.instance.TutorialActive())
        {
            if (!Tutorial.instance.grabedRope)
            {
                Tutorial.instance.GrabedOntoRope();
            }
        }

        Vector3 currVelocity = rb.velocity;
        connectToScript.connectToObject = sectionToCatch;
        rb.useGravity = false;
        moveOnRopeScript.currentPos = sectionPos;
        a.SetTrigger("OnRope");
        a.SetBool("CurrOnRope", true);
        sectionToCatch.GetComponent<Rigidbody>().AddForce(currVelocity, ForceMode.Impulse);
    }

    private void resetSectionToCatch()
    {
        sectionToCatch = null;
    }
}
