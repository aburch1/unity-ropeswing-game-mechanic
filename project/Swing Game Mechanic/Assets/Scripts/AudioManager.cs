﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance = null;

    public float masterVolumeSlider { get; set; }
    public float musicVolumeSlider { get; set; }
    public float sfxVolumeSlider { get; set; }

    [SerializeField]
    private AudioMixer masterMixer;

    private AudioSource bgMusic;
    [SerializeField]
    private AudioClip[] bgMusicClips;

    private Scene currentScene;

	// Use this for initialization
	void Start () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        masterVolumeSlider = 0.8f;
        musicVolumeSlider = 0.8f;
        sfxVolumeSlider = 0.8f;

        bgMusic = GetComponent<AudioSource>();
        bgMusic.clip = bgMusicClips[0];
        bgMusic.Play();
	}

    void Update()
    {
        if (SceneManager.GetActiveScene() != currentScene)
        {
            currentScene = SceneManager.GetActiveScene();

            SceneChanged(currentScene);
        }
    }

    private void SceneChanged(Scene newScene)
    {
        if (bgMusic.clip != bgMusicClips[newScene.buildIndex])
        {
            bgMusic.Stop();
            bgMusic.clip = bgMusicClips[newScene.buildIndex];
            bgMusic.Play();
        }
    }

    public void SetMasterVolume(float volume)
    {
        masterVolumeSlider = volume;
        float newVol = volume * 100f;
        newVol = -80 + newVol;
        masterMixer.SetFloat("MasterVol", newVol);
    }

    public void SetMusicVolume(float volume)
    {
        musicVolumeSlider = volume;
        float newVol = volume * 100f;
        newVol = -80 + newVol;
        masterMixer.SetFloat("MusicVol", newVol);
    }

    public void SetSFXVolume(float volume)
    {
        sfxVolumeSlider = volume;
        float newVol = volume * 100f;
        newVol = -80 + newVol;
        masterMixer.SetFloat("SFXVol", newVol);
    }
}
